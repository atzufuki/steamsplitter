export NVIDIA_DRIVER_VERSION=$(modinfo nvidia | grep ^version | xargs | sed s/version://g | xargs)
wget http://us.download.nvidia.com/XFree86/Linux-x86_64/${NVIDIA_DRIVER_VERSION}/NVIDIA-Linux-x86_64-${NVIDIA_DRIVER_VERSION}.run -O NVIDIA-DRIVER.run

export PATH_TO_SCRIPT=$(pwd)'/src/on_controller.py'
envsubst < steamsplitter.rules.tmpl > $out/lib/udev/rules.d/steamsplitter.rules

echo "{\"players\": []}" > ./src/state.json

docker build . -t steamsplitter:latest
