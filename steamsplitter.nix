{ stdenv, fetchgit }:

let
  traceLog = "/tmp/steam-trace-dependencies.log";
  version = "0.0.1";

in stdenv.mkDerivation {
  name = "steamsplitter";

  src = fetchgit {
    url = "https://gitlab.com/atzufuki/steamsplitter.git";
    rev = "e1edf68da535f1a4e53bff99fbd2c4c856412754";
    sha256 = "0xd1n99kgs4ky8s3fmds25256kdccb4fyy7nfg4biy6crk4y5frq";
  };

  installPhase = ''
    ./install.sh
  '';

  meta = with stdenv.lib; {
    description = "Split screen system for multiplayer sessions on a single machine.";
    homepage = "https://gitlab.com/atzufuki/steamsplitter/";
  };
}
