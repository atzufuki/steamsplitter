#!/bin/sh

NAMEOFYOURIMAGE="steamsplitter:latest"

xhost local:root
set -e
USER_UID=$(id -u)
USER_GID=$(id -g)

docker run -ti \
--rm \
--name='steamsplitter-test' \
-e "USER_UID=$USER_UID" \
-e "USER_GID=$USER_GID" \
--ipc='host' \
--device /dev/nvidia-modeset:/dev/nvidia-modeset \
--device /dev/nvidia-uvm:/dev/nvidia-uvm \
--device /dev/nvidia0:/dev/nvidia0 \
--device /dev/nvidiactl:/dev/nvidiactl \
--device /dev/dri/card0:/dev/dri/card0 \
--device /dev/dri/renderD128:/dev/dri/renderD128 \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v /dev/shm:/dev/shm \
--privileged=true \
-v /run/user/$USER_UID/pulse:/run/user/1000/pulse \
-e PULSE_SERVER=unix:/run/user/1000/pulse/native \
-e DISPLAY=$DISPLAY \
--env DBUS_SESSION_BUS_ADDRESS="$DBUS_SESSION_BUS_ADDRESS" \
-v /run/user/1000/bus:/var/run/dbus/system_bus_socket \
-v steam:/home/steam/.local/share/Steam \
$NAMEOFYOURIMAGE \
bash -c "steam -login 'atzufuckedyou' -bigpicture"
