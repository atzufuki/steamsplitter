## Installation

- Get nvidia driver version `modinfo nvidia | grep ^version | xargs | sed s/version://g | xargs`.
- Make sure install.sh downloads the correct driver version.
- Run `sudo ./install.sh`.
- After install, test that Steam launches in container `./test.sh`.

## Usage

Since the install script added some rules to `/etc/udev/rules.d`, udev is now listening for Xbox One controller connections.
