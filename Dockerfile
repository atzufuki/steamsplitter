FROM ubuntu

ARG DEBIAN_FRONTEND=noninteractive

ENV NO_AT_BRIDGE=1
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  

USER root

RUN apt-get update && apt-get install -y locales locales-all

# Steam dependencies

RUN apt-get update && apt-get install -yq \
    wget \
    curl \
    sudo \
    zenity \
    xterm \
    libnss3 \
    python3 \
    python3-apt \
    pciutils \
    dbus \
    dbus-x11 \
    file \
    pulseaudio

RUN dpkg --add-architecture i386 && apt-get update && apt-get -yq install \
    libc6-i386 \
    libgl1:i386 \
    libgl1-mesa-glx:i386 \
    libxtst6:i386 \
    libxrandr2:i386 \
    libglib2.0-0:i386 \
    libgtk2.0-0:i386 \
    libpulse0:i386 \
    libva2:i386 \
    libbz2-1.0:i386 \
    libvdpau1:i386 \
    libva-x11-2:i386 \
    libcurl4-gnutls-dev:i386 \
    libopenal1:i386 \
    libsm6:i386 \
    libice6:i386 \
    libasound2-plugins:i386 \
    libsdl2-image-2.0-0:i386

RUN apt-get install -yq gdebi-core

# Steam install
RUN wget -O ~/steam.deb http://media.steampowered.com/client/installer/steam.deb
RUN gdebi --option=APT::Get::force-yes=1,APT::Get::Assume-Yes=1 -n ~/steam.deb
# RUN wget http://media.steampowered.com/client/installer/steam.deb
# RUN dpkg -i steam.deb && apt-get install -f && dpkg -i steam.deb

# Steam user
RUN adduser --disabled-password --gecos 'Steam' steam && adduser steam video && adduser steam audio && adduser steam voice
RUN echo 'steam ALL = NOPASSWD: ALL' > /etc/sudoers.d/steam && chmod 0440 /etc/sudoers.d/steam

# NVIDIA dependencies
RUN apt-get -y update && apt-get -y upgrade && apt-get install -yq kmod mesa-utils libglvnd-dev \
    xserver-xorg-video-intel libgl1-mesa-glx libgl1-mesa-dri xserver-xorg-core

# NVIDIA driver install
COPY NVIDIA-DRIVER.run /tmp/NVIDIA-DRIVER.run
RUN sh /tmp/NVIDIA-DRIVER.run -a -N --ui=none --no-kernel-module --run-nvidia-xconfig
RUN rm /tmp/NVIDIA-DRIVER.run

USER steam
ENV HOME /home/steam
VOLUME /home/steam
CMD ["steam -bigpicture"]
