#!/bin/python3

import os
import sys
import json
import subprocess
from datetime import datetime

path = os.path.dirname(os.path.realpath(__file__))
statefile_path = f'{path}/state.json'
args = sys.argv
action = args[1]
dev_path = args[2]
dev_name = args[3]

player = {
    'id': dev_path.split('/')[4],
}

def parse_dev_name():
    if 'js' in dev_name:
        player['js'] = dev_name
    elif 'event' in dev_name:
        player['event'] = dev_name

def update_or_add_player(player):
    state = get_state()
    new = True
    updated_player = None
    for index, p in enumerate(state['players']):
        if player['id'] == p['id']:
            new = False
            updated_player = {**p, **player}
            state['players'][index] = updated_player
    if new:
        state['players'].append(player)
    set_state(state)
    msg = f'Xbox One controller connected at {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}'
    send_notify(msg)
    return updated_player

def start_container_for_player(player):
    js_name = player.get('js', None)
    device_name = None 
    os.system(f'echo "FFFFOOO" >>/tmp/scripts.log')
    if js_name:
        device_name = js_name.split('/')[-1]
    event_name = player.get('event', None)
    docker_ps = subprocess.check_output(['docker', 'ps'])
    if device_name and event_name and not js_name in str(docker_ps):
        os.system(f'su atzufuki -c "{path}/run.sh {device_name} {js_name} {event_name}"')

def remove_player():
    state = get_state()
    state['players'] = [x for x in state['players'] if not (player['id'] == x['id'])]
    set_state(state)
    msg = f'Xbox One controller disconnected at {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}'
    send_notify(msg)

def send_notify(msg):
    os.environ['DISPLAY'] = ':0'
    os.environ['DBUS_SESSION_BUS_ADDRESS'] = 'unix:path=/run/user/1000/bus'
    os.system(f'su atzufuki -c "notify-send \'{msg}\'"') 
    os.system(f'echo "{msg}" >>/tmp/scripts.log')

def get_state():
    f = open(statefile_path, "r")
    state = json.load(f)
    f.close()
    return state

def set_state(state):
    f = open(statefile_path, "w")
    f.write(json.dumps(state))
    f.close()

try:
    parse_dev_name()
    if action == 'add':
        updated_player = update_or_add_player(player)
        if updated_player:
            start_container_for_player(updated_player)
    elif action == 'remove':
        remove_player()
except Exception as e:
    os.system(f'echo "{e}" >>/tmp/scripts.log')

# "i3-msg split horizontal"